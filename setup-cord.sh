#!/bin/sh

set -x

if [ -z "$EUID" ]; then
    EUID=`id -u`
fi
if [ $EUID -ne 0 ] ; then
    echo "This script must be run as root" 1>&2
    exit 1
fi

# Grab our libs
. "`dirname $0`/setup-lib.sh"

if [ -f $OURDIR/cord-done ]; then
    exit 0
fi

logtstart "cord"

cd $OURDIR

curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
mv kubectl /usr/local/bin
chmod 755 /usr/local/bin/kubectl

wget https://storage.googleapis.com/kubernetes-helm/helm-v2.12.1-linux-amd64.tar.gz
tar -xzvf helm-v2.12.1-linux-amd64.tar.gz
mv linux-amd64/helm /usr/local/bin/helm

helm init --upgrade --force-upgrade
kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'
helm init --service-account tiller --upgrade
while [ 1 ]; do
    helm ls
    if [ $? -eq 0 ]; then
	break
    fi
    sleep 4
done
helm repo add cord https://charts.opencord.org
helm repo update
helm install -n cord-platform cord/cord-platform --version=6.1.0

# Wait until all pods are Running.
echo "Waiting for CORD platform pods..."
while [ 1 ] ; do
    count=`kubectl get pods | grep -vE "Running|NAME" | wc -l`
    if [ -n "$count" -a $count -eq 0 ]; then
	echo "All CORD platform pods running."
	break
    fi
    sleep 8
done

logtend "cord"
touch $OURDIR/cord-done
