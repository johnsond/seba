#!/bin/sh

set -x

if [ -z "$EUID" ]; then
    EUID=`id -u`
fi
if [ $EUID -ne 0 ] ; then
    echo "This script must be run as root" 1>&2
    exit 1
fi

# Grab our libs
. "`dirname $0`/setup-lib.sh"

if [ -f $OURDIR/seba-done ]; then
    exit 0
fi

logtstart "seba"

echo "Waiting for etcd CRDs to appear..."
while [ 1 ]; do
    count=`kubectl get crd | grep etcd | wc -l`
    if [ -n "$count" -a $count -ge 3 ]; then
	echo "Found $count etcd CRDs."
	break
    fi
    sleep 8
done

helm install -n seba cord/seba --version=1.0.0

# Wait until all pods are Running or Completed.
echo "Waiting for SEBA pods..."
while [ 1 ] ; do
    count=`kubectl get pods | grep -vE "Running|Completed|NAME" | wc -l`
    if [ -n "$count" -a $count -eq 0 ]; then
	echo "All SEBA pods running."
	break
    fi
    sleep 8
done

# Install the AT&T workflow.
helm install -n att-workflow cord/att-workflow --version=1.0.2

# Wait until all pods are Running or Completed.
echo "Waiting for AT&T workflow pods..."
while [ 1 ] ; do
    count=`kubectl get pods | grep -vE "Running|Completed|NAME" | wc -l`
    if [ -n "$count" -a $count -eq 0 ]; then
	echo "All AT&T workflow pods running."
	break
    fi
    sleep 8
done

logtend "seba"
touch $OURDIR/seba-done
